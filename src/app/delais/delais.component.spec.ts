import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelaisComponent } from './delais.component';

describe('DelaisComponent', () => {
  let component: DelaisComponent;
  let fixture: ComponentFixture<DelaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
