import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FILMS } from './films';
import { Film } from './film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) {}

  getFilms(): Promise<Film[]>{
    return Promise.resolve(FILMS);
  }

  getFilm(id: number): Promise<Film> {
    return Promise.resolve(FILMS.find(film => film.id==id));
  }

  getFilmsWithObservable(): Observable<any> {
    return this.http.get("http://localhost:4200/").map((response: Response) => response);
  }

   getCarWithObservable(id): Observable<any> {
      return this.http.get("http://localhost:4200/"+id).map((response: Response) => response);
   }
}