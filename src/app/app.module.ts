import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';
import { DelaisComponent } from './delais/delais.component';

const appRoutes: Routes = [
  {
    path: 'film-list',
    component: FilmsComponent,
    data: { title: 'Films List' }
  },
  { path: '',
    redirectTo: '/film-list',
    pathMatch: 'full'
  },
  {
    path: 'delais',
    component: DelaisComponent,
    data: { title: 'Delais' }
  },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    DelaisComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
