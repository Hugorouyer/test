export class Film{

    id: number;
    titre: string;
    duree: number;
    limitation_age: number;
    rented: boolean;
    date_location: string;
    duree_location: number;
}