import { Film } from './film';
export const FILMS: Film[] = [
  {id: 1,
    titre: 'Trainspotting',
    duree: 93,
    limitation_age: 16,
    rented: false,
    date_location: '',
    duree_location: 4,

  },
  {
    id: 2,
    titre: 'Drive',
    duree: 110,
    limitation_age: 12,
    rented: false,
    date_location: '',
    duree_location: 4,

  },
  {id: 3,
    titre: 'Interstellar',
    duree: 169,
    limitation_age: 0,
    rented: false,
    date_location: '',
    duree_location: 1,

  },
  {
    id: 4,
    titre: 'Sin City',
    duree: 123,
    limitation_age: 12,
    rented: false,
    date_location: '',
    duree_location: 4,

  },
  {
    id: 5,
    titre: 'Gladiator',
    duree: 155,
    limitation_age: 16,
    rented: false,
    date_location: '',
    duree_location: 5,

  },
  {
    id: 6,
    titre: 'Rollerball',
    duree: 125,
    limitation_age: 12,
    rented: false,
    date_location: '',
    duree_location: 10,

  },
  {
    id: 7,
    titre: 'Phantom Of The Paradise',
    duree: 92,
    limitation_age:12 ,
    rented: false,
    date_location: null,
    duree_location: null,

  },
  {
    id: 8,
    titre: 'Dune',
    duree: 140,
    limitation_age: 0,
    rented: false,
    date_location: null,
    duree_location: null,

  },
  {
    id: 9,
    titre: 'Fantastic Mr Fox',
    duree: 88,
    limitation_age: 0,
    rented: false,
    date_location: "27/01/2020",
    duree_location: 8,

  },
  {
    id: 10,
    titre: 'Blade Runner',
    duree: 117,
    limitation_age: 0,
    rented: false,
    date_location: null,
    duree_location: null,

  }

];
